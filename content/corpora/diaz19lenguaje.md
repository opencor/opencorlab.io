---
id: diaz19lenguaje
title: "Lenguaje Ofensivo en Redes Sociales: Hacia la Definición de Criterios Lingüísticos para su Detección Automática"
abstract:
    en: ""
contact:
    name: María José Díaz Torres
    email: "maria.diazto@udlap.mx"
authors_abstract:
    - María José Díaz-Torres
    - Paulina Morán-Méndez
    - Luis Villaseñor-Pineda
links: !!omap
    - pre-print: ""
year: 2019
type: text
category: methodology
languages:
    - Mexican Spanish
languages_codes:
    - es-MX
glottolog:
    - https://glottolog.org/resource/languoid/id/amer1254
date: 2019-10-08T08:00:00-00:00
showDate: true
draft: false
type: corpora
---

En la actualidad, las plataformas de redes sociales se han convertido en el
medio de comunicación y expresión por excelencia. Lamentablemente, y en gran
parte a que nuestro interlocutor puede permanecer anónimo, este medio también
se ha usado para ofender o difundir información falsa. Debido a la relevancia
y gravedad de este fenómeno, la comunidad ha empezado a buscar formas au-
tomáticas para detectar textos agresivos en redes sociales. Es con esta motivación
que surge el MEX-A3T “Authorship and aggressiveness analysis in Twitter: case
study in Mexican Spanish”, presentado originalmente en el workshop IberEval
2018. El MEX-A3T presenta una tarea de detección automática de tuits agre-
sivos u ofensivos escritos en español de México, lo cual supone un reto extra a
la clasificación al involucrar variación lingüı́stica, pero que, al mismo tiempo, la
acerca a la realidad. La dificultad de esta tarea se vio demostrada por el bajo
rendimiento reportado por la mayorı́a de los participantes en la primera edición
del MEX-A3T, y por ello se propuso exactamente la misma tarea para el Iber-
LEF 2019.

Con esta motivación, a través de la presente investigación se buscó definir los
principales rasgos lingüı́sticos que caracterizan al lenguaje ofensivo manifestado
a través de las redes sociales, con el objetivo de proponer un conjunto inicial
de criterios que faciliten su identificación. De igual forma, a través de estos cri-
terios será posible etiquetar sistemáticamente un corpus para esta tarea. Para
poder lograr lo anterior, fue necesario primero definir claramente los conceptos
de lenguaje ofensivo, agresivo y vulgar. Al tener claros los marcos de referencia,
entonces se revisaron y analizaron los corpus de tuits ofensivos con los cuales
cuenta el Laboratorio de Tecnologı́as del Lenguaje (LabTL) del INAOE. En
función de las observaciones realizadas, se adecuaron los marcos actuales, con
el fin de llegar a una caracterización. Durante este proceso se identificaron los
elementos léxicos y semánticos más representativos de los mensajes agresivos,
ofensivos o vulgares y ası́ se diseñó una tipologı́a que facilitó la categorización
de manera clara y visual al usar un diagrama de flujo. En este diagrama se in-
cluyeron los conceptos de vulgaridad, agresividad y ofensividad como cualidades
del mensaje. Finalmente, el recurso generado sirvió para hacer una revisión y
validación del corpus del MEX-A3T. Como resultado, el uso de este recurso au-
mentó el coeficiente kappa de confiabilidad inter-evaluador de 0.58 a 0.92 en el
conjunto de datos de entrenamiento de dicho corpus.

