---
id: freitas20petroles
title: "Petrolês: primeiros passos na construção de um corpus de domı́nio"
abstract:
    en: ""
corpus:
    name: "Petrolês"
    link: ""
authors_abstract:
    - Claudia Freitas
links: !!omap
    - pre-print: ""
year: 2020
type: text
category: resource
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - https://glottolog.org/resource/languoid/id/braz1246
date: 2020-03-01T11:00:00-00:00
showDate: true
draft: false
type: corpora
---


#### O Petrolês

Apresentamos aqui a versão 1.0 do corpus Petrolês, um corpus composto por
teses, dissertações e monografias que tematizam, de alguma maneira, a grande
área de petróleo. A intenção do Petrolês é se tornar um material de referência
para o PLN de lı́ngua portuguesa, tendo vista especialmente, mas não exclu-
sivamente, aplicações vinculadas à extração de informação na área de O&G.
A versão 1.0 do corpus conta com 399 documentos, totalizando 6.5 milhões de
unidades (tokens), distribuı́dos em cerca de 256 mil frases. Como um todo, o
corpus contém 10 vezes esse tamanho – 4 mil documentos.


##### Estrutura e organização

Cada documento tem associado os seguintes dados: tı́tulo, ano, autor, orienta-
dor, instituição, palavras-chave, resumo, figuras e tabelas. Exceto pelas figuras,
tabelas e resumos, armazenados à parte, as demais informações também estão
incluı́das como atributos estruturais nos documentos, ao lado de atributos lin-
guisticos como parágrafos e sentenças.
    Cada documento é disponibilizado no formato texto cru (texto simples já
tratado), associado ao seu documento oroginal (em pdf). Em versões futuras,
cada documento conterá anotações linguı́sticas relativas a classes de palavras
(pos), análise sintática, entidades mencionadas e relações entre entidades.


##### O quanto vale um pré-processamento?

Muito do trabalho com PLN vem sendo feito com textos que já nasceram em
formato eletrônico - notı́cias online, blogs, tweets. No Petrolês, o primeiro desafio
vem do formato dos textos, que estão em pdf, e por isso um pré-processamento
bem cuidado é fundamental para garantir o sucesso das etapas subsequentes.
    A versão 1.0 do Petrolês foi pré-processada por uma ferramenta desenvolvida
especialmente para dar conta dos problemas relativos à passagem de documen-
tos pdf para txt. Com isso, foram corrigidos erros de segmentação, hifenização,
cabeçalho e rodapé. Tabelas, figuras e fórmulas foram excluı́das dos documentos,
que contém apenas textos. Está em andamento o tratamento de tı́tulos e nomes
de seção.
    A tabela 1 apresenta alguns números antes e após o (pré)processamento, a
fim de ilustrar o impacto de um tratamento cuidadoso do texto. Como pode-
mos observar, é impressionante a diferença nos números: sem o tratamento, a
quantidade de tokens é quase três vezes maior, e a quantidade de palavras difer-
entes é quase o dobro (e, se desde 1994 [1] já nos alertam sobre a dificuldade
e relevância de tokenização e sentenciação para o PLN, por outro lado, essas
etapas/tarefas continuam sendo consideradas algo trivial e nada importante).
Quando lembramos que palavras são as unidades principais para o processa-
mento automático de textos, seja para anotação, seja para a geração de vetores
de palavras, percebemos a relevância de um material bem cuidado como etapa
inicial para um PLN bem sucedido.
    O material está disponı́vel em [anônimo].


Table 1. Comparação entre o corpus Petrolês v.1 antes e depois do pré-processamento

Versão do corpus | Frases | Tokens  |Palavras diferentes (lemas)
--- | --- | --- | ---
Txt sem tratamento | 961.709 | 19.553.538 | 343.451
Txt com tratamento | 256.452 | 6.533.980 | 134.016

References

1. Grefenstette, G., Tapanainen, P.: What is a word, What is a sentence? Problems
   of Tokenization. In: Proc. 3rd Conference on Computational Lexicography, COM-
   PLEX’94, pp. 79–87. (1994).
