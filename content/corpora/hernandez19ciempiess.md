---
id: hernandez19ciempiess
title: "The CIEMPIESS Proper-Names Pronouncing Dictionary"
abstract:
    en: ""
corpus:
    name: "The CIEMPIESS Proper-Names Pronouncing Dictionary"
    link: "https://mega.nz/#!NoZ3XY4Y!kROQmlt0tlhDZUngvauC7NnWi3HyDYD87jEUkyiRStE"
authors_abstract:
    - Carlos Daniel Hernández-Mena
links: !!omap
    - pre-print: ""
year: 2019
type: text
category: resource
languages:
    - Mexican Spanish
languages_codes:
    - es-MX
glottolog:
    - https://glottolog.org/resource/languoid/id/amer1254
date: 2019-10-08T11:00:00-00:00
showDate: true
draft: false
type: corpora
---


Transcriptions in the CIEMPIESS-PNPD are based on a phonetic alphabet
called Mexbet. Mexbet was design for the Spanish of Central Mexico and it
has several levels of granularity.

The CIEMPIESS-PNPD comes in two versions: Mexbet T29 and Mexbet
T66. Level T29 of Mexbet means that transcriptions are made using only 29
symbols of the entire Mexbet. Level T66 means that 66 symbols are used. To
learn more about the Mexbet alphabet see the documents of the d̈ocd̈irectory.

The CIEMPIESS-PNPD counts with four different lists: Names.list, Last-
names.list, Places.list and Unknown.list. One can use these lists to determine if
a proper name in the dictionary belogs to a name, last name or place. Every
element of the three first lists are included in the pronouncing dictionaries. The
elements of the Unknown.list are also included in the dictionaries but they don’t
belong to any other list .
