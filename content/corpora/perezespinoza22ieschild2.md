---
id: perezespinoza22iescchild
title: "IESC-Child 2: An Interactive Emotional Children’s Speech Corpus"
corpus:
    name: "IESC-Child-2"
    link: "https://drive.google.com/file/d/19aUkGLps2gTdEqlo6UtprzKR3q00b--A/view"
links:
    - licencia: "https://drive.google.com/file/d/19aUkGLps2gTdEqlo6UtprzKR3q00b--A/view"
authors_abstract:
    - Humberto Pérez-Espinosa
    - Benjamín Gutiérrez-Serafín
    - Juan Martínez-Miranda
    - Ismael E. Espinosa-Curiel
    - Daniel Fajardo-Delgado
    - Isabel G. Vázquez-Gómez 
links: !!omap
    - pre-print: ""
year: 2021
type: speech
category: resource
languages:
    - Mexican Spanish
languages_codes:
    - es-MX
glottolog:
    - https://glottolog.org/resource/languoid/id/amer1254
date: 2022-01-25T04:00:00-00:00
showDate: true
draft: false
type: corpora
---

The IESC-Child-2 database consists of 9,446 audio samples. These samples are labeled with five aspects: emotion, attitude, mental state, HMI phenomena, and pronounced phrase. The labels are included in a CSV file, where the file name and the corresponding tag are specified. Moreover, this database contains the results of the CPQ. At the end of the IESC-Child-2 labeling process, we obtained 47,230 records in the database (9,446 segments by five annotators). This means that we have a record for each evaluation made by the annotators. The answers to each of the five aspects evaluated in each sample are included in the same record. The data were paired to group the responses of each annotator belonging to the same sample. For the sake of simplicity, we obtained the final classes for the five labeling categories by a plurality vote across all the annotators.
