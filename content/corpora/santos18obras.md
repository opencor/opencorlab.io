---
id: santos18obras
title: "OBras: a fully annotated and partially human-revised corpus of Brazilian literary works in the public domain"
abstract:
    en: "OBras is an open corpus which can be downloaded from the Linguateca site and which was created in order to be the Brazilian counterpart of corpo Vercial, a corpus of public domain literary works from Portugal. Being a part of the AC/DC project, it is searchable from the Web, as well as regularly annotated with syntactic and semantic information, and featuring a new “edition” every two months. Syntactical annotation is provided by the PALAVRAS parser, while colour, clothing, body, emotions and speech are annotated with specific rules. We present in this text several ways of quantifying its content."
corpus:
    name: "Obras Brasileiras"
    link: "http://www.linguateca.pt/"
authors_abstract:
    - Diana Santos
    - Claudia Freitas
    - Eckhard Bick
links: !!omap
    - pre-print: ""
year: 2018
type: text
category: resource
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - https://glottolog.org/resource/languoid/id/braz1246
date: 2018-09-24T12:00:00-00:00
showDate: true
draft: false
type: corpora
---

#### The AC/DC project

Ever since 1999 Linguateca has developed the AC/DC project so that people
could interrogate annotated corpora in Portuguese, with ever increasing quality
of annotation, a wider genre palette, and more kinds of information, compare [2]
with [3].

Even though all material is fully available for querying, not all corpora in-
cluded in AC/DC can be distributed in their entirety, due to copyright limita-
tions. Literature is one of the genres included in AC/DC since its creation, but it
is especially prone to availability restrictions. This is why most literary corpora
only include old texts which are already in the public domain, or have restrictive
conditions, like COMPARA.

This is why, at least in a first phase, we invested in literature which was
already in the public domain, which basically spans, in what Brazil is concerned,
one century. We named the corpus “Obras Brasileiras” with acronym OBras, and
we are still in the process of adding more texts. http://www.linguateca.pt/OBras
shows the works included, together with their metadata and size in tokens, as
well as how to downnload it.


#### Corpus description

Through the several annotations one can describe a corpus on many levels. We
start by the description in terms of part-of-speech, as well as the size and variety
of proper names. Then we present the quantities of all different semantic data,
for version 5.3 of 18 June 2018, see [4] for the annotation.


Table 1. Quantification of OBras according to annotation fields. NB! We have not
included the multiword cases of colour, body and clothing for type counting

Annotation |    Tokens  |         Types (lemmas)
--- | --- | ---
Size | ca. 5 millions @ 151,676
Verbs | 842,736 | 17,134
Nouns | 965,805 | 26,426
Adjectives | 289,507 | 11,087
Proper names | 132,210 | 21,320
Colours | 11,932 | 258
Clothing | 10,395 | 208
Body | 54,762 | 242
Saying verbs | 78,219 | 825
Emotions | 132,336 | 2,185


<sup>*</sup>Thanks to all who have contributed with text preparation and annotation, under
the scope of Linguateca.


References

1. Bick, Eckhard: The Parsing System “Palavras”: Automatic Grammatical Analysis of
   Portuguese in a Constraint Grammar Framework. Dr.phil. thesis. Aarhus University.
   Aarhus University Press, Aarhus, Denmark (2000)
2. Santos, Diana, Bick, Eckhard: Providing Internet access to Portuguese corpora:
   the AC/DC project. In: Gavrilidou, Maria et al. (eds.), Proceedings of the Second
   International Conference on Language Resources and Evaluation (LREC 2000), pp.
   205-210. ELRA (2010)
3. Santos, Diana: Corpora at Linguateca: Vision and Roads Taken. In: Berber
   Sardinha, Tony, Ferreira, Telma de Lurdes São Bento (Eds.), Working with Por-
   tuguese Corpora, pp. 219-236. Bloomsbury (2014 )
4. Anotacedil
           ¸ ão. http://www.linguateca.pt/acesso/anotacao.html. Last accessed 29
   July 2018

