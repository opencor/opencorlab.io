---
id: trigo21ptlanka
title: "PtLanka: an online corpus of Sri Lanka Portuguese lexicon and phonology"
abstract:
    en: ""
corpus:
    name: "PtLanka"
    link: "https://github.com/Portophon/PtLanka"
authors_abstract:
    - Luís Trigo
    - Carlos Silva
year: 2021
category: resource
type_: corpus
languages:
    - Sri Lankan Portuguese Creole
languages_codes:
    - idb-LK
glottolog:
    - http://glottolog.org/resource/languoid/id/mala1544
date: 2021-12-02T05:00:00-00:00
showDate: true
draft: false
type: corpora
---

The PtLanka database (https://github.com/Portophon/PtLanka) assembles a
total of 2524 words of Sri Lanka Portuguese from its most complete
dictionary (Dalgado1900), which covers the lexicon from the extinct dialect
of Colombo from the Sinhala-dominant region, and make it available online in
a consistent format.
