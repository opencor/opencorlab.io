---
id: wick_pedro21satiricorpus
title: "SatiriCorpus.Br - a corpus of satirical news for Brazilian Portuguese"
abstract:
    en: ""
corpus:
    name: "SatiriCorpus.Br"
authors_abstract:
    - Gabriela Wick-Pedro
    - Roney Santos
    - Oto Vale
year: 2021
category: resource
type_: corpus
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - http://glottolog.org/resource/languoid/id/braz1246
date: 2021-12-02T05:00:00-00:00
showDate: true
draft: false
type: corpora
---

The analysis of deceptive content is not only a challengingtask for society, but
in recent years Natural Language Processing (NLP)has been leaning towards
creating resources to automate the detectionof this type of content. Satirical
news, despite being considered as mis-leading content, is not intended to
mislead people, as is the case withother types of false news. However, they can
intentionally create a falsebelief of truth in the minds of the most inattentive
readers or without acontextual and cultural foundation. Thus, we present in this
abstract, wepresent SatiriCorpus.Br, a satirical news corpus for Brazilian
Portuguese.

