---
title: Accepted talks OpenCor 2019
date: 2019-10-1
---

* *AIRA: Acoustic Interactions for Robot Audition*<br/>
  _Caleb Rascón e Ivette Velez_.

* *The CIEMPIESS Proper-Names Pronouncing Dictionary*</br>
  _Carlos Daniel Hernández Mena_.

* *Corpus Reacción: consumers engagement in Facebook posts*<br/>
  _Erika Sarai Rosas-Quezada, Gabriela Ramírez-de-la-Rosa y Esau Villatoro-Tello_

* *IESC-Child: An Interactive Emotional Children's Speech Corpus*</br>
  _Humberto Pérez-Espinosa, Juan Martínez-Miranda, Ismael Espinosa-Curiel, Josefina Rodríguez Jacobo y Luis Villaseñor-Pineda_

* *B2W-Reviews01 - A Brazilian Portuguese reviews corpus*</br>
  _Livy Real, Marcio Oshiro, Alexandre Mafra_

* *Towards a twitter corpus of the indigenous languages of the Americas*</br>
  _Mónica Jasso Rosales, Manuel Mager e Ivan Vladimir Meza Ruiz_.

* *Construcción del Corpus Periodístico del Noroeste de México (COPENOR)*</br>
  _Manuel Alejandro Sanchez Fernandez y Alfonso Medina Urrea_.

* *Lenguaje Ofensivo en Redes Sociales: Hacia la Definición de Criterios Lingüísticos para su Detección Automática*</br>
  _María José Díaz-Torres, Paulina Alejandra Morán-Méndez y Luis Villaseñor-Pineda_.

