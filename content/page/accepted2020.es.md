---
title: Platicas aceptadas OpenCor 2020
date: 2020-03-01
---

* *C-ORAL-BRASIL y its resources*</br>
Heliana Mello, Tommaso Raso, Lucia Ferrari, Eckhard Bick y Henrique Chaves

* *Corpus de Referência do Português Contemporâneo: freely available subcorpora*</br>
Amália Mendes

* *Carvalho, um corpus diacrónico em ortografia original para português, espanhol e inglês*</br>
José Ramom Pichel, Pablo Gamallo Otero, Marco Neves y Iñaki Alegria

* *Pre-trained Portuguese BERT models*</br> 
Fábio Souza, Rodrigo Nogueira y Roberto Lotufo

* *Brands.Br – a Portuguese Reviews Corpus*</br>
Evandro Fonseca, Amanda Oliveira, Carolina Gadelha y Valter H. Guandaline

* *Petrolês: primeiros passos na construção de um corpus de domínio*</br>
Claudia Freitas

* *The English-Portuguese Parallel Corpus*</br>
Valter Martins y Larissa Freitas

* *B2W-Reviews02, an annotated review sample*</br>
Livy Real, Alissa Bento, Karina Soares, Marcio Oshiro y Alexandre Mafra
