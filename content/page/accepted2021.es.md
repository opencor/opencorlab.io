---
title: Lista de pláticas aceptadas  OpenCor 2021
date: 2020-03-01
---

*   _The Multilingual Corpus of Survey Questionnaires_ </br>
    Danielly Sorato and Diana Zavala-Rojas

*   _PtLanka: an online corpus of Sri Lanka Portuguese lexicon and phonology_ </br>
    Luís Trigo and Carlos Silva

*   _Building an annotated Nheengatu-Portuguese parallel corpus_</br>
    Juliana L. Gurgel, Dominick M. Alexandre and Leonel F. Alencar.

*   _MorphoBr: Um Dicionário Aberto de Alta Cobertura de Formas Plenas para Análises Morfológicas do Português_  </br>
    Ana Luiza Nunes, Leonel Figueiredo de Alencar, Alexandre Rademaker and
    Wellington José Leite da Silva

*   _The Catalan Language CLUB_</br>
    Carlos Rodríguez-Penagos, Carme Armentano-Oller, Marta Villegas, Maite Melero, Aitor González-Agirre, Ona de Gibert and Casimiro Carrino.

*   _Towards FraCaS-BR_ </br>
    Valeria de Paiva and Livy Real

*   _SatiriCorpus.Br - a corpus of satirical news for Brazilian Portuguese_</br>
    Gabriela Wick-Pedro, Roney Santos and Oto Vale

*   _Anonymization of the B2W-Reviews01 corpus_ </br>
    Fernando Zagatti, Lucas Silva and Livy Coelho

*   _Corpora do Calendário de Saúde_ </br>
    Leonardo Coelho and Larissa Freitas

*   _Etiquetagem morfossintática automática de corpora de língua falada transcrita_</br>
    Mônica Rigo Ayres
