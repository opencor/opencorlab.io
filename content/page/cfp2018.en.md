---
title: OpenCor 2018
date: 2018-09-17
---

## Motivation

Recent years have seen a move in Computational Linguistics towards bigger and better, more reliably annotated corpora. However, the existence of such reliably annotated corpora is one of the big bottlenecks for processing natural language. Producing and maintaining corpora is a hard task that most of the time requires sizeable funding and the cooperation of several experts. Although having such corpora available is clearly essential, the many difficulties and the amount of work needed to produce reliable corpora make the process of producing this data and making it available a non-trivial proposition. While “big data” is a trend, producing reliable corpora continues to be an invisible task in Natural Language Processing. Especially when working on languages different from English, on smaller datasets not immediately suitable for machine learning approaches or on a new release of a previous dataset, it is not obvious to the corpora creators how to publish and properly discuss their work. Most of the biggest Natural Language Processing venues are not open to accepting corpora descriptions. The situation is even worse when considering minority languages and endangered languages since most of them do not have a related venue where these works can be discussed. The Latin American and Iberian communities that produce open corpora do not have an established event that would make possible for experts to share ideas, to discuss difficulties and to get feedback on their work. Different meetings have been held in the last years, but either they are not generic enough to embrace all corpora work done in these communities, or there was no continuation or support for future editions. Due to these conditions, it is no rare that groups that share related interests or face the same difficulties are not aware of other groups and their recent work within these communities.

This forum aims both to fill the gap of having a permanent venue for construction, annotation, and maintenance of open corpora for Latin American and Iberian languages and to create an extensive list of these resources. OpenCor welcomes discussions on Portuguese, Spanish, indigenous languages, creoles, Galician, Catalan, Aragonese, Astur-Leonese, Aranese and any other language spoken in Latin America and Iberian countries.  Work on endangered languages, minority, and/or less resourced languages are particularly welcome.

## The venue

This is the first edition of OpenCor Forum, an attempt to gather the community that produces, maintains and makes freely available language resources for the large variety of languages spoken in Iberian countries and in Latin America. All accepted works will also be part of the OpenCor list, an initiate to have cataloged open resources produced for the targeted languages.
This forum welcomes, but it is not restricted to, the following topics:

* releases of new open data sets
* descriptions of established open corpora
* guidelines creation, annotation strategies, and best practices discussion
* corpora maintenance and management
* corpora curation and assessment
* corpora design and evaluation
* corpora creation strategies and difficulties faced by the community


## Submission

We invite submissions of anonymized extended abstracts up to 2 pages. Documents must follow the same style as PROPOR 2018 (Springer LNCS) and must be submitted in English. Submissions should begin with an abstract of 200 words or less. Accepted extended abstracts will be made available in the forum web page for a short period. OpenCor is non-archival, therefore works that have been or are planned to be published elsewhere are also welcome.

Authors need to submit together with the extended abstract the link for their resources. One of the goals of OpenCor is to provide a full list of resources and described languages by the end of the forum. We hope this list will be helpful in keeping track of freely available resources for our targeted languages.

Considering that one of the main challenges for these communities is funding raising, all accepted works will be available in the forum page and will appear in the resources list, even if no author can attend the forum. This is an attempt to create an extensive list of open corpora available that does not rely on how much funding the working groups have. In the moment of the submission, authors would be requested if their work should be considered for a talk/poster. We encourage authors that know they won’t be present at the forum but want to have their work in OpenCor List, to send to the organizers a one-page hand-out that will be available to the forum participants during the session.
 

Submission link: https://easychair.org/conferences/?conf=opencor2018 </br>
[LaTex stylesheet](ftp://ftp.springernature.com/cs-proceeding/llncs/llncs2e.zip) </br>
[MS Word stylesheet](ftp://ftp.springernature.com/cs-proceeding/llncs/word/splnproc1703.zip)</br>
 

## Dates


*Deadline submission*: July 31August 2</br>
*Acceptance*: August 10</br>
*Camera-ready version*: September 10</br>
*Session*: September, 24</br>

### Forum Registration
The participation in the forum is free of charge for participants of PROPOR. The fee for attendees who are not participants of the main conference is the Workshop/Tutorial fee. For authors who want to have their resources listed in OpenCor List but are not planning to attend the forum, the submission is free of charge.
Registration Link: http://www.inf.ufrgs.br/propor-2018/registration/
 

### Organization

* Livy Real – University of São Paulo / GLiC
* Ivan Vladimir Meza –  Universidad Nacional Autónoma de Mexico / IIMAS

### Program Committee

* Alfonso Medina Urrea
* Alexandre Maciel
* Aline Villavicencio
* Carlos Daniel Hernández Mena
* Diana Santos
* Erick Fonseca
* Fernanda López
* Gemma Bel-Enguix
* Gerardo Sierra
* Ivan Vladimir Meza Ruiz
* Jorge García
* Jose Ramom Pichel
* Kevin Scannell
* Livy Real
* Lucía Golluscio
* Manuel Mager
* Maria Jose Bocorny Finatto
* Néstor Becerra
* Pablo Gamallo
* Thiago Pardo
* Valeria de Paiva
* Ximena Gutierrez-Vasques

## Contact

Any question may be sent to the organizers: livyreal [at] gmail.com; ivanvladimir [at] turing.iimas.unam.mx



