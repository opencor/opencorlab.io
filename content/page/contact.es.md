---
title: Contacto
type: page
date: 2018-09-17
---

Cualquier pregunta puede ser enviada a los organizadores: livyreal [at] gmail.com; 
ivanvladimir [at] turing.iimas.unam.mx

Si quieres seguir la discutión y las noticias sobre Open Corpora en la región uneté
[a la lista de correos](https://groups.google.com/forum/#!forum/opencor).

También nos puedes seguir en [Twitter](https://twitter.com/OpenCorF).
