---
title: faq
description: Frequent Ask Questions
date: 2018-09-17
---

**¿What is the non archival modality of the forum?**

The non archival modality means that the whole paper is not to be considered published but a preprint. It will available for a small amount of time (2 months) but after that the link will stop working.

**¿What can I do with the paper?**

The feedback provided during the forum should help authors to improve their paper. With this improvement authors are free to submit the work any place they find suited for it.

**¿If there is not published paper how can I cite the work?**

As an abstract, you can associate it to the OpenCor Forum.

**¿What is the role of the corpora list?**

OpenCor forum aims to create an extensive list of the Open Corpora for the Latin American and Iberian Languages. Altough the paper is not considered published the reported corpus will be listed in the corpora list to facilitate its discovery and promotion.
