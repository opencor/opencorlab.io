---
title: faq
description: Preguntas frecuentes
date: 2018-09-17
---

**¿Qué significa la modalidad no _archival_ del foro forum?**

Significa que el trabajo presentado no es considerado publicado sino un borrador. 

**¿Qué puedo hacer con el artículo después?**

La retroalimentación proporcionada en tu participación deberá se de apoyo para escribir un artículo en extenso. Con este los autores deben sentirse libres de presentarlo en el foro que mejor les parezca. 

**¿Sino hay trabajo publicado como puedo citar el trabajo presentado en el foro?**

Como resumen, lo puedes asociar al Foro OpenCor.

**¿Cúal es el rol de la lista de corpus?**

El foro de OpenCor tiene como objetivo crear una lista extensiva los corpus libres de Latinoamérica y lenguajes ibéricos. Aunque el artículo no se considere publicado el corpus reportado va a ser listado para facilitar su descubriendo y promoción. 
