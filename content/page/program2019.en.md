
---
title: Program 2019
description: Guanjuato, Guanajuato, as a part of 4th PLAGAA 
date: 2019-10-01
---

October 8th, 2019


<table>
<col width="20%">
<col width="80%">
<tr>
<td>10:15 AM</td><td><strong>Sesion OpenCor</strong><br/><small>20 minuts presentation y 5 questions</small></td>
</tr>
<tr>
<td></td><td>
<em>Construcción del Corpus Periodístico del Noroeste de México (COPENOR)</em><br/>
<small><u>Manuel Alejandro Sanchez Fernandez</u> y Alfonso Medina Urrea<br/></small></td>
</tr>
<tr>
<td></td><td>
<em>Towards a twitter corpus of the indigenous languages of the Americas</em><br/>
<small><u>Mónica Jasso Rosales</u>, Manuel Mager y Ivan Vladimir Meza Ruiz</td></small>		
</tr>
<tr>
<td></td><td>
<em>Lenguaje Ofensivo en Redes Sociales: Hacia la Definición de Criterios Lingüísticos parasu Detección Automática</em></br>
<small><u>María José Díaz-Torres</u>, <u>Paulina Alejandra Morán-Méndez</u> y Luis Villaseñor-Pineda</small>
</td>
</tr>
<tr>
<td>11:30 AM</td><td><strong>Video proyection</strong>
</tr>
<tr>
<td>11:45 AM</td><td><strong>Coffee break</strong>
</tr>
<tr>
<td>12:00 PM</td><td>
<em><strong>CLOE: Corpus de Lengua Oral del Español</strong></em><br/>
<small><strong>Fernanda López (Plática Invitada)</strong></small></td>
</tr>
<tr>
<td>12:30 PM</td><td><strong>Sesión OpenCor</strong><br/>
<small>20 minutos de presentación y 5 de preguntas</small></td>
</tr>
<tr>
<td></td><td>
<em>Corpus Reacción: consumers engagement in Facebook posts</em></br>
<small>Erika Sarai Rosas-Quezada, <u>Gabriela Ramírez-de-la-Rosa</u> y Esau Villatoro-Tello</small></td>
</tr>
<tr>
<td></td><td>
<em>AIRA: Acoustic Interactions for Robot Audition</em></br>
<small>Caleb Rascón and <u>Ivette Velez</u></small></td>
</tr>
<tr>
<td></td><td>
<em>IESC-Child: An Interactive Emotional Children's Speech Corpus</em><br/>
<small><u>Humberto Pérez-Espinosa</u>, Juan Martínez-Miranda, Ismael Espinosa-Curiel, Josefina Rodríguez Jacobo y Luis Villaseñor-Pineda</small><td/>
</tr>
<tr>
<td>1:45 am</td><td><strong>Conclussions</strong>
</tr>
</table>



