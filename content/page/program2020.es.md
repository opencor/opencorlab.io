
---
title: Programa 2020
description: Evora, Portugal, as a part of PROPOR
date: 2020-03-01
---

Marzo 20nd, 2020

March 2, 2020. Colégio Espírito Santo, University of Evora. Room 120

* **9h30-9h50** Carvalho, um corpus diacrónico em ortografia original para português, espanhol e inglês, *José Ramom Pichel, Pablo Gamallo Otero, Marco Neves and Iñaki Alegria*
* **9h50-9h55** Video: Brands.Br – a Portuguese Reviews Corpus, *Evandro Fonseca, Amanda Oliveira, Carolina Gadelha and Valter H. Guandaline*
* **9h55-10:20** B2W-Reviews01-Opinion, an annotated review sample, *Livy Real, Alissa Bento, Karina Soares, Marcio Oshiro and Alexandre Mafra*
* **10h20-10h25** Video: Pre-trained Portuguese BERT models, *Fábio Souza, Rodrigo Nogueira and Roberto Lotufo*
* **10h25-10h55** Petrolês: primeiros passos na construção de um corpus de domínio, *Cláudia Freitas*
* **10h55-11h** Video: English-Portuguese parallel corpus made of song lyrics, *Valter Martins and Larissa Freitas*

* **11h-11h30** Coffee Break

* **11h30-12h** Corpus de Referência do Português Contemporâneo: freely available subcorpora, *Amália Mendes*
* **12h-13h** Invited Speaker: An overview of open language resources for Galician, *Marcos Garcia*


