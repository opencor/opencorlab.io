---
title: Program 2021
description: STIL 2021 - Symposium in Information and Human Language Technology (Online)
date: 2021-12-02
---

December 2nd, 2021

| Time | Title |  Authors |
|---|---|---|
|15:00 | Etiquetagem morfossintática automática de corpora de língua falada transcrita | Mônica Rigo Ayres |
|15:05 | MorphoBr: Um Dicionário Aberto de Alta Cobertura de Formas Plenas para Análises Morfológicas do Português | Ana Luiza Nunes, Leonel Figueiredo de Alencar, Alexandre Rademaker and Wellington José Leite da Silva |
|15:10 | Anonymization of the B2W-Reviews01 corpus | Fernando Zagatti, Lucas Silva and Livy Coelho |
|15:15 | Corpora do Calendário de Saúde | Leonardo Coelho and Larissa Freitas |
|15:20 | SatiriCorpus.Br - a corpus of satirical news for Brazilian Portuguese | Gabriela Wick-Pedro, Oto Vale and Roney Santos |
|15:25-16:00 |Discussion | |


| Time | Title |  Authors |
|---|---|---|
| 15:00	| Etiquetagem morfossintática automática de corpora de língua falada transcrita	| Mônica Rigo Ayres |
| 15:05	| MorphoBr: Um Dicionário Aberto de Alta Cobertura de Formas Plenas para Análises Morfológicas do Português	| Ana Luiza Nunes, Leonel Figueiredo de Alencar, Alexandre Rademaker and Wellington José Leite da Silva  |
| 15:10	| Anonymization of the B2W-Reviews01 corpus	| Fernando Zagatti, Lucas Silva and Livy Coelho |
| 15:15	| Corpora do Calendário de Saúde | 	Leonardo Coelho and Larissa Freitas	 |
| 15:20	| SatiriCorpus.Br - a corpus of satirical news for Brazilian Portuguese | Gabriela Wick-Pedro, Oto Vale and Roney Santos	|
| 15:25-16:00 | Discussion	| |


| Time | Title |  Authors |
|---|---|---|
| 16:00-16:45 | A importância de ser Ernesto: dados abertos em português | Valéria de Paiva	|
| 16:45-17:00 |	Discussion	||


